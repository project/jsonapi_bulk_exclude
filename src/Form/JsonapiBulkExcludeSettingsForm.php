<?php

namespace Drupal\jsonapi_bulk_exclude\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\ProxyClass\Routing\RouteBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure JSON:API Bulk Exclude settings for this site.
 */
class JsonapiBulkExcludeSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected $routerBuilder;

  /**
   * The Entity Type Manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs the form object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\ProxyClass\Routing\RouteBuilder $router_builder
   *   The router builder to rebuild menus after saving config entity.
   */
  public function __construct(ConfigFactoryInterface $config_factory, RouteBuilder $router_builder, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($config_factory);
    $this->routerBuilder = $router_builder;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('router.builder'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['jsonapi_bulk_exclude.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'jsonapi_bulk_exclude_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('jsonapi_bulk_exclude.settings');

    $types_string = '';
    $types_array = $config->get('excluded_fields');
    if (!empty($types_array)) {
      $types_string = implode("\n", $types_array);
    }

    $form['excluded_fields'] = [
      '#title' => $this->t('Fields'),
      '#type' => 'textarea',
      '#required' => FALSE,
      '#size' => 15,
      '#description' => $this->t('The selected fields will be excluded from the JSON:API responses. Use the public names, e.g. node_type.'),
      '#default_value' => $types_string,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('jsonapi_bulk_exclude.settings')
      ->set('excluded_fields', array_filter(array_map('trim', explode("\n", $form_state->getValue('excluded_fields')))))
      ->save();

    // Rebuild the router.
    $this->routerBuilder->setRebuildNeeded();

    parent::submitForm($form, $form_state);
  }

}
