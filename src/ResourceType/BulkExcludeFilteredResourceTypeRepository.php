<?php

namespace Drupal\jsonapi_bulk_exclude\ResourceType;

use Drupal\jsonapi_extras\Entity\JsonapiResourceConfig;
use Drupal\jsonapi_extras\ResourceType\ConfigurableResourceTypeRepository;

class BulkExcludeFilteredResourceTypeRepository extends ConfigurableResourceTypeRepository {

  /**
   * @inheritDoc
   */
  protected function overrideFields(JsonapiResourceConfig $resource_config) {
    $fields = parent::overrideFields($resource_config);
    $excluded_fields = $this->configFactory->get('jsonapi_bulk_exclude.settings')->get('excluded_fields');
    if (!empty($excluded_fields)) {
      foreach ($fields as $key => $field) {
        if (in_array($field->getPublicName(), $excluded_fields)) {
          $fields[$key] = $field->disabled();
        }
      }
    }
    return $fields;
  }

}
